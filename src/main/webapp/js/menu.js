var menu =
"<ul id='menu'>"+
	"<li class=''><a href='#' id='doc_prc'>전표처리</a>"+
		"<ul>"+
			"<li class=''><a href='#' id='elc_doc'>전자증빙</a>"+
				"<ul>"+
					"<li class=''><a href='#' id='docPrc_elc_hello'>전자세금계산서</a></li>"+
					"<li class=''><a href='#' id='docPrc_elc_nello'>법인카드(개별)</a></li>"+
					"<li class=''><a href='#'>법인카드(일괄)</a></li>"+
					"<li class=''><a href='#'>가지급금/전도금 신청</a></li>"+
					"<li class=''><a href='#'>전자내부증빙(개별)</a></li>"+
					"<li class=''><a href='#'>전자내부증빙(일괄)</a></li>"+
					"<li class=''><a href='#'>원천세(전자)</a></li>"+
					"<li class=''><a href='#'>기타매출</a></li>"+
					"<li class=''><a href='#'>기타</a></li>"+
					"<li class=''><a href='#'>이메일 전자세금계산서</a></li>"+
				"</ul>"+
			"</li>"+
			"<li class=''><a href='#'>실물증빙</a>"+
				"<ul>"+
					"<li class=''><a href='#' id='docPrc_rel_relTaxBill'>실물세금계산서</a></li>"+
					"<li class=''><a href='#' id='docPrc_rel_relEtcDocSing'>실물기타증빙(개별)</a></li>"+
					"<li class=''><a href='#' id='docPrc_rel_relEtcDocWhol'>실물기타증빙(일괄)</a></li>"+
					"<li class=''><a href='#'>실물지로</a></li>"+
					"<li class=''><a href='#'>원천세(실물)</a></li>"+
					"<li class=''><a href='#'>가지급금 반제</a></li>"+
					"<li class=''><a href='#'>기타매출</a></li>"+
					"<li class=''><a href='#'>부가세/AP 전표</a></li>"+
					"<li class=''><a href='#'>AR/부가세 전표</a></li>"+
					"<li class=''><a href='#'>구매처수익 전표</a></li>"+
					"<li class=''><a href='#'>기타</a></li>"+
				"</ul>"+
			"</li>"+
			"<li class=''><a href='#'>SAP전표 결재상신</a>"+
				"<ul>"+
					"<li class=''><a href='#'>전자세금계산서</a></li>"+
					"<li class=''><a href='#'>법인카드개별</a></li>"+
				"</ul>"+
			"</li>"+
		"</ul>"+
	"</li>"+	
	
	"<li class=''><a href='#'>전표관리</a>"+
		"<ul>"+
			"<li class=''><a href='#'>전자증빙</a>"+
				"<ul>"+
					"<li class=''><a href='#'>전자세금계산서</a></li>"+
					"<li class=''><a href='#'>법인카드개별</a></li>"+
					"<li class=''><a href='#'>법인카드개별2</a></li>"+
					"<li class=''><a href='#'>법인카드개별2</a></li>"+
				"</ul>"+
			"</li>"+
			"<li class=''><a href='#'>전자증빙</a>"+
				"<ul>"+
					"<li class=''><a href='#'>전자세금계산서</a></li>"+
					"<li class=''><a href='#'>법인카드개별</a></li>"+
				"</ul>"+
			"</li>"+
		"</ul>"+
	"</li>"+ //1tier li
	
	"<li class=''><a href='#'>법인카드관리</a>"+
		"<ul>"+
			"<li class=''><a href='#'>카드발급 및 한도변경</a>"+
			"</li>"+
			"<li class=''><a href='#'>보유카드 정보조회</a>"+
			"</li>"+
		"</ul>"+
	"</li>"+ //1tier li
	
	"<li class=''><a href='#'>전자결재승인</a>"+
		"<ul>"+
			"<li class=''><a href='#'>전표승인</a>"+
			"</li>"+
			"<li class=''><a href='#'>법인카드 발급승인</a>"+
			"</li>"+
		"</ul>"+
	"</li>"+ //1tier li

	"<li class=''><a href='#'>승인권한관리</a>"+
		"<ul>"+
			"<li class=''><a href='#'>위임</a>"+
			"</li>"+
			"<li class=''><a href='#'>위임 History</a>"+
			"</li>"+
		"</ul>"+
	"</li>"+ //1tier li	
	

	"<li class=''><a href='#'>기능</a>"+
		"<ul>"+
			"<li class=''><a href='#' id='function_home'>Home</a>"+
			"</li>"+
			"<li class=''><a href='#' id='function_login'>Login</a>"+
			"</li>"+
			"<li class=''><a href='#' id='function_docList'>DocList</a>"+
			"</li>"+
			"<li class=''><a href='#' id='function_fb51'>FB51</a>"+
			"</li>"+
		"</ul>"+
	"</li>"+
	
"</ul>";  //top est

